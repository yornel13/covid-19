

export interface Cases {
    state?: string;
    total?: number;
    confirmed?: number;
    deaths?: number;
}

export const opts = {
    inGraphDataShow: true,
    inGraphDataRadiusPosition: 2,
    inGraphDataFontColor: 'white',

    inGraphDataTmpl: '<%=v6+\' %\'%>',
    inGraphDataFontFamily: '\'Open Sans\'',
    inGraphDataFontStyle: 'normal bold',
    inGraphDataFontSize: 12,
    inGraphDataPaddingX: 0,
    inGraphDataPaddingY: -5,
    inGraphDataAlign: 'center',
    inGraphDataVAlign: 'top',
    inGraphDataXPosition: 2,
    inGraphDataYPosition: 3,
    inGraphDataAnglePosition: 2,
    inGraphDataRotate: 0,
    inGraphDataPaddingAngle: 0,
    inGraphDataPaddingRadius: 0,
    inGraphDataBorders: false,
    inGraphDataBordersXSpace: 1,
    inGraphDataBordersYSpace: 1,
    inGraphDataBordersWidth: 1,

    legend: true,
    legendBlockSize: 15,
    legendFillColor: 'rgba(255,255,255,0.00)',
    legendColorIndicatorStrokeWidth: 1,
    legendPosX: -2,
    legendPosY: 4,
    legendXPadding: 0,
    legendYPadding: 0,
    legendBorders: false,
    legendBordersWidth: 1,
    legendBordersStyle: 'solid',
    legendBordersColors: 'rgba(102,102,102,1)',
    legendBordersSpaceBefore: 5,
    legendBordersSpaceLeft: 5,
    legendBordersSpaceRight: 5,
    legendBordersSpaceAfter: 5,
    legendSpaceBeforeText: 5,
    legendSpaceLeftText: 5,
    legendSpaceRightText: 5,
    legendSpaceAfterText: 5,
    legendSpaceBetweenBoxAndText: 5,
    legendSpaceBetweenTextHorizontal: 5,
    legendSpaceBetweenTextVertical: 5,
    legendFontFamily: '\'Open Sans\'',
    legendFontStyle: 'normal normal',
    legendFontColor: 'rgba(0,0,0,1)',
    legendFontSize: 15,
};

