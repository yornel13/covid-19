import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { CasesService } from '../../services/cases.service';
import { Cases, opts } from '../../models/cases.model';
import { Chart } from 'chart.js';

@Component({
    selector: 'app-cases',
    templateUrl: 'cases.page.html',
    styleUrls: ['cases.page.scss']
})
export class CasesPage implements OnInit, AfterViewInit {

    @ViewChild('graphicCanvas', {static: false}) graphicCanvas: ElementRef;
    private graphic: Chart;

    dataCases: any = [];
    states: string[] = [];
    cases: Cases = {};
    lastUpdated: Date;

    loading = true;
    showStates = false;

    constructor(private casesService: CasesService) {
        this.resetCases();
    }

    ngOnInit() {
        this.getCases();
    }
    ngAfterViewInit() {
    }

    getCases() {
        this.casesService.getCases().subscribe(data => {
            console.log(data);
            // @ts-ignore
            this.dataCases = data.cities;
            // @ts-ignore
            this.lastUpdated = new Date(data.lastUpdate);
            this.setStates();
            this.setGlobalData();
        });
    }

    setGlobalData() {
        this.resetCases();
        this.cases.state = 'Venezuela';
        for (const caseByState of this.dataCases) {
            if (caseByState.cases) {
                this.cases.total += caseByState.cases;
                this.cases.confirmed += caseByState.cases;
            }
            if (caseByState.deaths) {
                this.cases.total += caseByState.deaths;
                this.cases.deaths += caseByState.deaths;
            }
        }
        console.log(this.cases);
        this.states.sort();
        this.loading = false;
        this.createGraphic();
    }

    setStateData(state: string) {
        this.resetCases();
        this.cases.state = state;
        this.dataCases.filter(caseByState => {
            if (caseByState.state === state) {
                if (caseByState.cases) {
                    this.cases.total += caseByState.cases;
                    this.cases.confirmed = caseByState.cases;
                }
                if (caseByState.deaths) {
                    this.cases.total += caseByState.deaths;
                    this.cases.deaths = caseByState.deaths;
                }
            }
        });
        this.showStates = false;
        this.createGraphic();
    }

    showByStates(action) {
        if (action) {
            this.showStates = true;
        } else {
            this.showStates = false;
            this.setGlobalData();
        }
    }

    createGraphic() {
        setTimeout(() => {
            if (this.graphicCanvas) {
                this.graphic = new Chart(this.graphicCanvas.nativeElement, {
                    type: 'doughnut',
                    data: {
                        datasets: [
                            {
                                label: 'Cases Graphic of Covid-19',
                                data: [this.cases.confirmed, this.cases.deaths],
                                backgroundColor: [
                                    '#428cff',
                                    '#ff4961',
                                ]
                            }
                        ]
                    },
                    options: {},
                });
            }
        }, 100);
    }


    resetCases() {
        this.cases = {state: '', total: 0, confirmed: 0, deaths: 0};
    }
    setStates() {
        this.dataCases.map(caseByState => this.states.push(caseByState.state));
    }

}
