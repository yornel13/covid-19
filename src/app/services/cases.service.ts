import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class CasesService {

    url = 'https://coronavirus-cities-api.now.sh/country/';

    constructor(private http: HttpClient) {
    }

    getCases(country = 'venezuela') {
        return this.http.get(this.url + country);
    }

}
